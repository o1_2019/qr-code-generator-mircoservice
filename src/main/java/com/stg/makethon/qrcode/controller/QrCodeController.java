package com.stg.makethon.qrcode.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stg.makethon.qrcode.service.QRCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class QrCodeController {
    private final QRCodeService qrCodeService;


    @Autowired
    public QrCodeController(QRCodeService qrCodeService) {
        this.qrCodeService = qrCodeService;
    }

    @PostMapping("/api/qrcode/new")
    public ResponseEntity<byte[]> generateQrCode(@RequestBody Object data) {
        ObjectMapper mapper = new ObjectMapper();
        String dataJson = null;
        try {
            dataJson = mapper.writeValueAsString(data);
            System.out.println(dataJson);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(qrCodeService.generateQrCode(dataJson), HttpStatus.OK);
    }
}

package com.stg.makethon.qrcode.service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;

@Service
@Cacheable(cacheNames = "qrcode-cache", sync = true)
public class QRCodeService {
    public byte[] generateQrCode(String data) {
        byte[] qrcodeBytes = null;
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BitMatrix bitMatrix = new MultiFormatWriter().encode(data, BarcodeFormat.QR_CODE, 256, 256);
            MatrixToImageWriter.writeToStream(bitMatrix, MediaType.IMAGE_PNG.getSubtype(), outputStream);
            MatrixToImageWriter.writeToPath(bitMatrix, MediaType.IMAGE_PNG.getSubtype(), FileSystems.getDefault().getPath("image.png"));
            qrcodeBytes = outputStream.toByteArray();
        } catch (WriterException | IOException e) {
            e.printStackTrace();
        }
        return qrcodeBytes;
    }

    @CacheEvict(cacheNames = "qrcode-cache", allEntries = true)
    public void purgeCache() {
        System.out.println("Purging all cache");
    }
}
